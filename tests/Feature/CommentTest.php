<?php

namespace Tests\Feature;

use App\Models\Article;
use App\Models\Comment;
use App\Models\User;
use Cassandra\Type\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Tests\TestCase;

class CommentTest extends TestCase
{
    use WithFaker;

    /**
     * @var \Illuminate\Database\Eloquent\Collection|User
     */
    private $user;

    /**
     * @var \Illuminate\Database\Eloquent\Collection|Comment
     */
    private $comments;

    /**
     * @var \Illuminate\Database\Eloquent\Collection|Article
     */
    private $article;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = \App\Models\User::factory()->create();
        $this->article = \App\Models\Article::factory()->for($this->user)->create();
        $this->comments = Comment::factory()->count(5)->for($this->user)->for($this->article)->create();
    }

    public function test_unauthorized_user_can_see_index()
    {
        $response = $this->getJson(route('comments.index'));
        $response->assertSuccessful();
    }

    public function test_unauthorized_user_can_not_see_show()
    {
        $comment = $this->comments->random();
        $response = $this->getJson(route('comments.show',['comment' => $comment]));
        $response->assertUnauthorized();
    }

    public function test_unauthorized_user_can_not_create_comments()
    {
        $data = [
            'body' => $this->faker->paragraph,
            'user_id' => $this->user->id,
            'article_id' => $this->article->id,
        ];
        $request = $this->postJson(route('comments.store'), $data);
        $request->assertUnauthorized();
    }

    public function test_unauthorized_user_can_not_delete_comments()
    {
        $comment = $this->comments->random();
        $response = $this->deleteJson(route('comments.destroy',['comment' => $comment]));
        $response->assertUnauthorized();
    }

    public function test_authorized_user_can_save_comments()
    {
        $comment = $this->comments->random();
        Passport::actingAs(
            $this->user,
            [route('comments.store', ['comment' => $comment])]
        );
        $data = [
            'body' => $this->faker->paragraph,
            'user_id' => $this->user->id,
            'article_id' => $this->article->id,
        ];
        $response = $this->postJson(route('comments.store',$data));
        $response->assertSuccessful();
    }

    public function test_authorized_user_can_delete_comments()
    {
        $comment = $this->comments->random();
        Passport::actingAs(
            $this->user,
            [route('comments.destroy', ['comment' => $comment])]
        );
        $response = $this->deleteJson(route('comments.destroy',['comment' => $comment]));
        $response->assertSuccessful();
    }

    public function test_comment_validation_error_no_body()
    {
        $comment = $this->comments->random();
        Passport::actingAs(
            $this->user,
            [route('comments.store', ['comment' => $comment])]
        );
        $data = [
            'user_id' => $this->user->id,
            'article_id' => $this->article->id,
        ];
        $request = $this->postJson(route('comments.store'), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('body', (array)$payload->errors);
    }

    public function test_comment_validation_error_min()
    {
        $comment = $this->comments->random();
        Passport::actingAs(
            $this->user,
            [route('comments.store', ['comment' => $comment])]
        );
        $data = [
            'body' => '',
            'user_id' => $this->user->id,
            'article_id' => $this->article->id,
        ];
        $request = $this->postJson(route('comments.store'), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('body', (array)$payload->errors);
    }
}
