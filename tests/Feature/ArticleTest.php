<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ArticleTest extends TestCase
{
    use WithFaker;

    /**
     * @var Collection|User
     */
    private $user;

    /**
     * @var Collection|Model
     */
    private $articles;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = \App\Models\User::factory()->create();
        $this->articles = \App\Models\Article::factory()->count(10)->for($this->user)->create();
    }

    /**
     * Failed create article.
     * @group articles
     * @return void
     */
    public function test_unauthorized_user_can_see_all_articles()
    {
        $request = $this->getJson(route('articles.index'));
        $request->assertSuccessful();
    }

    /**
     * Failed create article.
     * @group articles
     * @return void
     */
    public function test_unauthorized_user_can_not_delete_article()
    {
        $article = $this->articles->random();
        $request = $this->deleteJson(route('articles.destroy',['article' => $article]));
        $request->assertUnauthorized();
    }

    /**
     * Failed create article.
     * @group articles
     * @return void
     */
    public function test_unauthorized_user_can_not_see_article()
    {
        $article = $this->articles->random();
        $request = $this->getJson(route('articles.show',['article' => $article]));
        $request->assertUnauthorized();
    }

    /**
     * Failed create article.
     * @group articles
     * @return void
     */
    public function test_unauthorized_user_can_not_save_articles()
    {
        $data = [
            'content' => $this->faker->paragraph(6),
            'user_id' => $this->user->id,
        ];
        $request = $this->postJson(route('articles.store'), $data);
        $payload = json_decode($request->getContent());
        $request->assertUnauthorized();
    }

    /**
     * Failed create article.
     * @group articles
     * @return void
     */
    public function test_title_validation_error_no_title()
    {
        $article = $this->articles->random();
        Passport::actingAs(
            $this->user,
            [route('articles.store',['article' => $article])]
        );
        $data = [
            'content' => $this->faker->paragraph(6),
            'user_id' => $this->user->id,
        ];
        $request = $this->postJson(route('articles.store'), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('title', (array)$payload->errors);
    }

    /**
     * Failed create article.
     * @group articles
     * @return void
     */
    public function test_title_validation_error_no_content()
    {
        $article = $this->articles->random();
        Passport::actingAs(
            $this->user,
            [route('articles.store',['article' => $article])]
        );
        $data = [
            'title' => $this->faker->paragraph(6),
            'user_id' => $this->user->id,
        ];
        $request = $this->postJson(route('articles.store'), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('content', (array)$payload->errors);
    }

    /**
     * Failed create article.
     * @group articles
     * @return void
     */
    public function test_title_validation_error_no_user_id()
    {
        $article = $this->articles->random();
        Passport::actingAs(
            $this->user,
            [route('articles.store',['article' => $article])]
        );
        $data = [
            'title' => $this->faker->paragraph(1),
            'content' => $this->faker->paragraph(6),
        ];
        $request = $this->postJson(route('articles.store'), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('user_id', (array)$payload->errors);
    }

    /**
     * Failed create article.
     * @group articles
     * @return void
     */
    public function test_title_validation_error_title_min()
    {
        $article = $this->articles->random();
        Passport::actingAs(
            $this->user,
            [route('articles.store',['article' => $article])]
        );
        $data = [
            'title' => 'qw',
            'content' => $this->faker->paragraph(6),
            'user_id' => $this->user->id,
        ];
        $request = $this->postJson(route('articles.store'), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('title', (array)$payload->errors);
    }

    /**
     * Failed create article.
     * @group articles
     * @return void
     */
    public function test_title_validation_error_content_min()
    {
        $article = $this->articles->random();
        Passport::actingAs(
            $this->user,
            [route('articles.store',['article' => $article])]
        );
        $data = [
            'title' => 'qwerty',
            'content' => 'asd',
            'user_id' => $this->user->id,
        ];
        $request = $this->postJson(route('articles.store'), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('content', (array)$payload->errors);
    }

    /**
     * Failed create article.
     * @group articles
     * @return void
     */
    public function test_validation_error_no_data()
    {
        $article = $this->articles->random();
        Passport::actingAs(
            $this->user,
            [route('articles.store',['article' => $article])]
        );
        $data = [];
        $request = $this->postJson(route('articles.store'), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('title', (array)$payload->errors);
        $this->assertArrayHasKey('content', (array)$payload->errors);
        $this->assertArrayHasKey('user_id', (array)$payload->errors);
    }

    /**
     * Success get all articles
     * @group articles
     * @return void
     */
    public function test_can_get_all_articles()
    {
        $request = $this->getJson(route('articles.index'));
        $request->assertSuccessful();
        $payload = json_decode($request->getContent());
        $this->assertArrayHasKey('data',(array)$payload);
        $this->assertCount($this->articles->count(),$payload->data);
    }

    public function test_get_article()
    {
        $article = $this->articles->random();
        $request = $this->getJson(route('articles.show',['article' => $article]));
        $request->assertUnauthorized();
    }

    /**
     * Failed create article.
     * @group articles
     * @return void
     */
    public function test_success_get_article()
    {
        $article = $this->articles->random();
        Passport::actingAs(
            $this->user,
            [route('articles.show',['article' => $article])]
        );
        $request = $this->getJson(route('articles.show',['article' => $article]));
        $request->assertSuccessful();
        $payload = json_decode($request->getContent());
        $this->assertArrayHasKey('data',(array)$payload);
        $this->assertArrayHasKey('id',(array)$payload->data);
        $this->assertEquals($article->id,$payload->data->id);
    }

    /**
     * Failed create article.
     * @group articles
     * @return void
     */
    public function test_authorized_user_can_save_articles()
    {
        $article = $this->articles->random();
        Passport::actingAs(
            $this->user,
            [route('articles.store',['article' => $article])]
        );
        $data = [
            'title' => $this->faker->title,
            'content' => $this->faker->paragraph(6),
            'user_id' => $this->user->id,
        ];
        $request = $this->postJson(route('articles.store'), $data);
        $request->assertSuccessful();
    }

    /**
     * Failed create article.
     * @group articles
     * @return void
     */
    public function test_authorized_user_can_delete_articles()
    {
        $article = $this->articles->random();
        Passport::actingAs(
            $this->user,
            [route('articles.destroy',['article' => $article])]
        );
        $request = $this->deleteJson(route('articles.destroy',['article' => $article]));
        $request->assertSuccessful();
    }
}
